<?php
/**
 * Palette Plugin
 *
 * @package         Palette
 * @author          hellofromTonya
 * @license         GPL2+
 * @link            https://gitlab.com/uptechlabs/Palette
 *
 * @wordpress-plugin
 * Plugin Name:     Fulcrum Plugin
 * Plugin URI:      https://gitlab.com/uptechlabs/Palette
 * Description:     Palette - the theming platform for WordPress to make customizing, updating, and switching themes a delightful experience.
 * Version:         1.0.0
 * Author:          hellofromTonya
 * Author URI:      https://UpTechLabs.io
 * Text Domain:     palette
 * Requires WP:     4.5
 * Requires PHP:    5.4
 */

/*
	This program is free software; you can redistribute it and/or
	modify it under the terms of the GNU General Public License
	as published by the Free Software Foundation; either version 2
	of the License, or (at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

namespace UpTechLabs\Palette;

if ( ! defined( 'ABSPATH' ) ) {
	exit( 'Cheatin&#8217; uh?' );
}


/**
 * Setup the plugin's constants.
 *
 * @since 1.0.0
 *
 * @return void
 */
function init_constants() {
	$plugin_url = plugin_dir_url( __FILE__ );
	if ( is_ssl() ) {
		$plugin_url = str_replace( 'http://', 'https://', $plugin_url );
	}

	define( 'PALETTE_PLUGIN_URL', $plugin_url );
	define( 'PALETTE_PLUGIN_DIR', __DIR__ . '/' );
}

/**
 * Launch the plugin.
 *
 * @since 1.0.0
 *
 * @return void
 */
function launch() {
	init_constants();

	require_once( __DIR__ . '/assets/vendor/autoload.php' );

//	$controller = new Controller();
}
